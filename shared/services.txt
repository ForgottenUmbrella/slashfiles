# Networking
NetworkManager.service
nftables.service
systemd-resolved.service

# Hardware
avahi-daemon.service
cups.socket
fstrim.timer

# Sysadmin
paccache.timer
reflector.timer
systemd-boot-update.service

# Desktop
gdm.service
systemd-oomd.service
