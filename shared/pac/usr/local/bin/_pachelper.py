#!/usr/bin/env python
"""
Helper for the pac command to update declarative configuration as root.

Not intended to be run by users.
"""

from collections.abc import Container, Iterable
import fileinput
import os
from pathlib import Path
import re
import subprocess
import sys


def main(argv: list[str]) -> None:
    """Entry point."""
    [_, command_name, config_dir, config_name, *packages] = argv
    match command_name:
        case 'add':
            command = add_record
        case 'remove':
            command = remove_record
        case _:
            raise ValueError(f'Unknown command "{command_name}"')
    config_dir = Path(config_dir)

    config_dir.mkdir(parents=True, exist_ok=True)
    config_path = config_dir/config_name
    command(config_path, packages)
    edit(config_path)


def add_record(config: Path, packages: Iterable[str]) -> None:
    """Add packages/services to config record."""
    def is_comment(line: str) -> bool:
        return line.startswith('#')

    def with_newline(line: str) -> str:
        return f'{line}\n'

    unsorted_comment = '# Unsorted\n'
    # Open in a+ mode to create the file if it doesn't already exist.
    with open(config, 'a+') as f:
        f.seek(0)
        new_packages = set(packages)
        last_comment = None
        for line in f:
            new_packages.discard(strip_inline_comment(line).strip())
            if is_comment(line):
                last_comment = line
        if last_comment != unsorted_comment:
            f.writelines(['\n', unsorted_comment])
        f.writelines(map(with_newline, sorted(new_packages)))


def remove_record(config: Path, packages: Container[str]) -> None:
    """Remove packages/services from config record."""
    with fileinput.input(config, inplace=True) as f:
        for line in f:
            if strip_inline_comment(line).strip() not in packages:
                sys.stdout.write(line)


def edit(config: Path) -> None:
    """Open config file in editor to organise packages/services."""
    subprocess.run(['sudoedit', str(config)], check=False)


def strip_inline_comment(line: str) -> str:
    """Remove trailing comments from a line."""
    return re.sub(r'#.*$', '', line)


if __name__ == '__main__':
    main(sys.argv)
